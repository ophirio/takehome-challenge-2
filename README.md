# Programming exercise - re-inventing promises

> A Promise is a proxy for a value not necessarily known when the promise is created. It allows you to associate handlers with an asynchronous action's eventual success value or failure reason. This lets asynchronous methods return values like synchronous methods: instead of immediately returning the final value, the asynchronous method returns a promise to supply the value at some point in the future.

# The problem

## Setup

- Fork the repository to your own account
- Clone it to your local machine
- Create a branch

## What to do

Complete the implementation of the Promise class found in `src/index.js` so that it satisfies the test suite.

Your implementation should be generic enough to not only pass the test suite, but to be used like the native Promise library.

## Testing

To run the test suite, run:

```bash
npm test
```

## What we're looking for

- Logical commit history
    - Dont just do everything in a single commit and submit it. Because this is a problem that you can do at your leisure, we want to see a commit history that is logical as you complete the problem.
- Coding best practices
- No use of third party libraries
    - They're not needed to solve this problem. We want to see you solve it with the tools found natively in ES6

## When you're finished...

Create a pull/merge request from your fork back to the original repository.