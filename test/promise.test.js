const Promise = require('../src');
const assert = require('assert');

describe('promises', function() {
	describe('basics', function() {
		it('should create a new promise using the constructor and resolve', function(done) {
			new Promise((resolve) => {
				resolve(1234);
			})
			.then((value) => {
				assert.strictEqual(value, 1234);
				done();
			});
		});
		it('should create a new promise using the constructor and reject', function(done) {
			new Promise((resolve, reject) => {
				reject(new Error('rejected'));
			})
			.catch((err) => {
				assert.strictEqual(err.message, 'rejected');
				done();
			});
		});
		it('should create a promise by using the static .resolve() function', function(done) {
			Promise.resolve(1234)
			.then((value) => {
				assert.strictEqual(value, 1234);
				done();
			});
		});
		it('should create a promise by using the static .reject() function', function(done) {
			Promise.reject(new Error('rejected static'))
			.catch((err) => {
				assert.strictEqual(err.message, 'rejected static');
				done();
			});
		});
	});


	describe('chaining', function() {
		it('should chain promises from a constructor', function(done) {
			new Promise((resolve) => {
				resolve(1234);
			})
			.then((value) => value + 1)
			.then((value) => value + 1)
			.then((value) => value + 1)
			.then((value) => {
				assert.strictEqual(value, 1237);
				done();
			});
		});
		it('should chain promises using the static resolve function', function(done) {
			Promise.resolve(1234)
			.then((value) => value + 1)
			.then((value) => value + 1)
			.then((value) => value + 1)
			.then((value) => {
				assert.strictEqual(value, 1237);
				done();
			});
		});
		it('should reject then resolve a promise', function(done) {
			Promise.reject(new Error('something'))
			.catch(() => 1234)
			.then((value) => value + 1)
			.then((value) => value + 1)
			.then((value) => {
				assert.strictEqual(value, 1236);
				done();
			});
		});
		it('should properly catch an error in a promise chain', function(done) {
			Promise.resolve(new Error('something'))
			.then(() => {
				return Promise.resolve()
				.then(() => {
					return Promise.resolve()
					.then(() => {
						throw new Error('deep error');
					});
				});
			})
			.catch((err) => {
				assert.strictEqual(err.message, 'deep error');
				done();
			});
		});
	});
	describe('Promise.all', function() {
		it('should properly resolve Promise.all with all resolved promises', function(done) {
			const numbers = [1, 2, 3, 4];
			Promise.all(numbers.map((num) => num * 2))
			.then((multipliedNumbers) => {
				assert.deepEqual([2, 4, 6, 8], multipliedNumbers);
				done();
			});
		});
		it('should reject when one rejects', function(done) {
			const numbers = [1, 2, 3, 4];
			Promise.all(numbers.map((num) => {
				if (num % 2 === 0) {
					return Promise.reject(new Error(num));
				}
			}))
			.catch((err) => {
				assert.strictEqual(err.message, '2');
				done();
			});
		});
	});

});