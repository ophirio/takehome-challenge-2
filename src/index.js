

class Promise {
	/**
	 * The Promise.resolve() method returns a Promise object that is resolved with a given value.
	 * @param value
	 * 	Argument to be resolved by this Promise. Can also be a Promise or a thenable to resolve.
	 */
	static resolve(value) {

	}

	/**
	 * The Promise.reject() method returns a Promise object that is rejected with a given reason.
	 * @param reason
	 * 	Reason why this Promise rejected.
	 */
	static reject(reason) {

	}

	/**
	 *
	 * @param executor(resolve, reject)
	 * 	A function that is passed with the arguments resolve and reject. The executor
	 * 	function is executed immediately by the Promise implementation, passing
	 * 	resolve and reject functions (the executor is called before the Promise
	 * 	constructor even returns the created object). The resolve and reject functions,
	 * 	when called, resolve or reject the promise, respectively. The executor normally
	 * 	initiates some asynchronous work, and then, once that completes, either calls
	 * 	the resolve function to resolve the promise or else rejects it if an error occurred.
	 * 	If an error is thrown in the executor function, the promise is rejected. The return
	 * 	value of the executor is ignored.
	 */
	constructor(executor) {

	}

	get state() {
		return this._state;
	}

	set state(state) {
		this._state = state;
	}

	/**
	 *
	 * @param onFulfilled
	 * 	A Function called if the Promise is fulfilled. This function has one argument,
	 * 	the fulfillment value. If it is not a function, it is internally replaced with
	 * 	an "Identity" function (it returns the received argument).
	 * @param onRejected
	 * 	A Function called if the Promise is rejected. This function has one argument,
	 * 	the rejection reason. If it is not a function, it is internally replaced with
	 * 	a "Thrower" function (it throws an error it received as argument).
	 *
	 * @return
	 * 	Once a Promise is fulfilled or rejected, the respective handler function
	 * 	(onFulfilled or onRejected) will be called asynchronously (scheduled in the
	 * 	current thread loop).
	 */
	then(onFulfilled, onRejected) {

	}


	/**
	 *
	 * @param onRejected
	 * 	A Function called when the Promise is rejected. This function has one argument:
	 * 		- reason:
	 * 			The rejection reason.
	 * 	The Promise returned by catch() is rejected if onRejected throws an error or returns a
	 * 	Promise which is itself rejected; otherwise, it is resolved.
	 *
	 * @return
	 * 	Internally calls Promise.prototype.then on the object upon which is called, passing
	 * 	the parameters undefined and the onRejected handler received; then returns the value
	 * 	of that call (which is a Promise).
	 */
	catch(onRejected) {

	}

	/**
	 *
	 * @param iterable - An iterable object such as an Array
	 * @returns
	 * 	- An already resolved Promise if the iterable passed is empty.
	 * 	- An asynchronously resolved Promise if the iterable passed contains no promises.
	 * 	- A pending Promise in all other cases.
	 */
	all(iterable = []) {

	}
}

module.exports = Promise;